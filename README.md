Group 7
Samrut Gadde, Hunter Ross, Rhea Shah, Pramit De, Vaishu Vasudevan

Substance-Abuse.me
Substance Abuse Victims


**Purpose:**

Helping raise awareness for various types of substance abuse and where people can go to get treated, as well as how they can get treated

**Model 1: Substances: 20-30 instances**

Attributes:
- Treatments
- Classification of drug
- Symptoms
- Fatality
- Age range most seen in
- Legality
- Alternate names


Media:
- Images
- Usage charts/trends


How instances will link:
- What substances a treatment center focuses on
- What cities see a lot of this drug


**Model 2: Treatment Centers: 50-100 instances**

Attributes:
- City
- Size
- State or private
- Minors vs adults
- Type of treatment they focus on

Media:
- Location on a map
- Picture of the center


How instances will link:
- What drugs this treatment center specializes in treating/types of treatment
- What city it’s in



**Model 3: Cities/Regions with High Drug Use:  20-30 cities**

Attributes:
- number of substance abuse victims
- Region in the US
- Type of common drug use
- Drug use growing/declining
- Density of treatment centers


Media:
- Map
- Pictures/slideshow

How instances will link:

- Treatment centers will link if they are contained in that region/city. 
- Substance instances will link if they are commonly abused in that region/city.

**Questions to answer:**
- What are some common substances people may get addicted to and what are the symptoms?
- Where can people go to get help, and how will they be helped?
- What substances are commonly abused in the reader’s city?


**URLs of Data Sources:**
URLs of Data Sources: 
- 	Healthcare.gov Post API: https://www.healthcare.gov/developers/
- 	Finding Treatment Centers: https://findtreatment.gov/locator
- 	Drug Use by City: https://americanaddictioncenters.org/blog/substance-abuse-by-city
- 	City Data: https://www.city-data.com/



